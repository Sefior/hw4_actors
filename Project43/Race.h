#pragma once
#include<unordered_map>
//TO DO: do something with feats; maybe create a new class for them 
//TODO: end with races!

class Race
{
private:
	std::string race_name;
	std::unordered_map<std::string, int> char_bonuses ={
		{"str",0},
		{"agi", 0},
		{"body",0},
		{"int",0},
		{"wis",0},
		{"char",0}
											};
	std::string feats;

public:
	Race() {};
	void show_feats()
	{
		std::cout << feats;
	}


	Race(std::string name, std::unordered_map<std::string,int> bonuses, std::string f)
	{
		race_name = name;
		feats = f;
		std::unordered_map<std::string, int>::iterator it1 = bonuses.begin();
		std::unordered_map<std::string, int>::iterator it2 = char_bonuses.begin();
		for (it1,it2; it1 != bonuses.end(); ++it1,++it2)
		{
			it2->second += it1->second;
		}
		
		

	};


	void show_race()
	{
		std::cout << race_name << "\n";
		std::cout << "Characteristic bonuses: " << std::endl;
		std::unordered_map<std::string, int>::iterator it = char_bonuses.begin();
		for (it; it != char_bonuses.end(); it++)
		{
			std::cout << it->first << " " << it->second << std::endl;
		}
		show_feats();

	}


};

//high elves, wood elves, and dark elves, who are commonly called drow

//Race HillDwarwes("Hill Df",
//	{
//	{ "str",0 },
//	{ "agi", 0 },
//	{ "body",2 },
//	{ "int",0 },
//	{ "wis",1 },
//	{ "Char",0 }
//	},
//	"Dark vision,Dwarven Resilience,Dwarven Combat Training,+1Hitpoint");

//Race MountDwarwes("Mountain Dwarf", { 2,0,2,0,0,0 }, "Dark vision,Dwarven Resilience,Dwarven Combat Training, Light and medium armour");
//
//Race HighElf("High Elf", { 0,2,0,1,0,0 }, "Keen Senses,Fey Ancestry,Trance");
//
//Race WoodElf("Wood Elf", { 0,2,0,0,1,0 }, "Keen Senses,Fey Ancestry,Trance");
//
//Race DarkElf("Dark Elf", { 0,2,0,0,0,1 }, "Keen Senses,Fey Ancestry,Trance");
//
