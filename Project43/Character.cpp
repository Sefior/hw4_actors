#include <iostream>
#include <unordered_map>
#include "Race.h"
#include <array>
#include "Classes.h"

class Character
{
protected:
	//std::array<std::string, 6> characteristic_names = { "Str","Agi","Body","Int","Wis","Cha" };
	//std::array<int, 6> char_values = { 15,14,13,12,10,8 };
	std::unordered_map<std::string, int> characteristics =
	{
		{"str",0},
		{"agi", 0},
		{"body",0},
		{"int",0},
		{"wis",0},
		{"char",0}
	};
	Race c_race;
	Character_class p_class;
	std::string name;
	int max_hp;
	int cure_hp;
	int AC;
	int initiative = 0;
	int profiency_bonus;
	int lvl = 1;
	std::unordered_map<std::string, int> skills;
	std::unordered_map < std::string, int> saves;
	std::unordered_map<std::string, bool> trained_saves;

public:
	Character() {};
	~Character() {};
	void getRace(Race r)
	{

	}
	
	void getProfBonus()
	{
		if (lvl >= 1 && lvl < 5)
			profiency_bonus = 2;
		else if (lvl >= 5 && lvl < 9)
			profiency_bonus = 3;
		else if (lvl >= 9 && lvl < 12)
			profiency_bonus = 4;
		else if (lvl >= 12 && lvl < 16)
			profiency_bonus = 5;
		else
			profiency_bonus = 6;
	}

};

//class Player_character :public Character
//{
//protected:
//	int lvl;
//	std::string char_class;
//	int death = 0;
//	int leave = 0;
//	int mastery_bonus;
//	int inspiration;
//	std::string race;
//public:
//	Player_character() {};
//	void m_create_character(Player_character* a)
//	{
//		std::cout << "Chose character race: ";
//	}
//};
