#include <time.h>
#include <iostream>
#include "DiceRoller.h"


class Dice
{
private:
	int edge;
public:
	Dice() {};
	int setEdge(int a)
	{
		edge = a;
	};
	int roll()
	{
		edge = rand() % edge + 1;
		return edge;
	};
};

int roll_d4()
{
	int res = rand() % 4 + 1;
	return res;
}

int roll_d6()
{
	int res = rand() % 6 + 1;
	return res;
}

int roll_d8()
{
	int res = rand() % 8 + 1;
	return res;
}

int roll_d10()
{
	int res = rand() % 10 + 1;
	return res;
}

int roll_d12()
{
	int res = rand() % 12 + 1;
	return res;
}

int roll_d20()
{
	int res= rand() % 20 + 1;
	return res;
}

int roll_d100()
{
	int a = rand() % 100+1;
	return a;
}