#include "Armour.h"
#include <time.h>
#include <string>
#include <iostream>


class Armour
{
private:
	std::string armour_name;
	int cost;
	int AC;
	int weight;
	bool equiped = false;

public:
	Armour(std::string name, int c, int ac, int w)
	{
		armour_name = name;
		cost = c;
		AC = ac;
		weight = w;
	}

	int getAC()
	{

		if (equiped == true)
			return AC;
		else
			return 0;
	}

	void show_armour()
	{
		std::cout <<"Armour name: "<< armour_name << std::endl;
		std::cout << "AC: " << AC << std::endl;
		std::cout << "Cost: " << cost << "  " << "Weight: " << weight << std::endl;
	}

	void equip() { equiped = true; }
	void disarm() { equiped = false; }

};


class Shield 
{
private:
	std::string shield_name;
	int AC_bonus = 2;
	bool active = true;
public:
	Shield(std::string n, int acb)
	{
		shield_name = n;
		AC_bonus = acb;

	}

	void add_shield()
	{
		active = true;
		
	}

	void disarm()
	{
		active = false;
		
	}

	int get_shield_bonus()
	{
		if (active == true)
			return 2;
		else
			return 0;
	}

};