#include <iostream>
#include <unordered_map>
#include <string>
#include "Classes.h"
#include "DiceRoller.cpp"



//TODO: make trained skills(may be map<string,bool>)

std::string ClasTypes[4] = {
	"Barbarian",
	"Warior",
	"batler",
	"Caster"
};

class Character_class
{
private:
	std::string name;
	std::string class_type;
	int start_hp;
	Dice hp_dices;
	bool heavy_armour_training = false;
	bool medium_armour_training = false;
	bool light_armour_training = false;
	bool warior_weapon_training = false;
	bool simple_weapon_training = false;
	bool caster = false;
	std::unordered_map<const std::string, int> skills =
	{
		{"Acrobatics",0},
		{"Animal Handling",0},
		{"Arcana",0},
		{"Athletics",0},
		{"Deception",0},
		{"History",0},
		{"Insight",0},
		{"Intimidation",0}
		{"Investigation",0}
		{"Medicine",0},
		{"Nature",0},
		{"Perception",0},
		{"Perfomance",0},
		{"Persuration",0},
		{"Religion",0},
		{"Sleight of Hands",0},
		{"Stealth",0},
		{"Surival",0},
	};
	std::unordered_map<const std::string, bool>trained_skills =
	{
	{ "Acrobatics",0 },
	{ "Animal Handling",0 },
	{ "Arcana",0 },
	{ "Athletics",0 },
	{ "Deception",0 },
	{ "History",0 },
	{ "Insight",0 },
	{ "Intimidation",0 }
	{"Investigation", 0}
	{"Medicine", 0},
	{ "Nature",0 },
	{ "Perception",0 },
	{ "Perfomance",0 },
	{ "Persuration",0 },
	{ "Religion",0 },
	{ "Sleight of Hands",0 },
	{ "Stealth",0 },
	{ "Surival",0 },
	};
	int mastery_bonus = 2; //some classes have up on different lvl
	int lvl;
public:
	Character_class(std::string Cname, std::string Ctype)
	{
		name = Cname;
		class_type = Ctype;
		if (class_type == ClasTypes[0])
		{
			start_hp = 12;
			hp_dices.setEdge(12);
			medium_armour_training = true;
			light_armour_training = true;
			simple_weapon_training = true;
			warior_weapon_training = true;
		}

		else if (class_type == ClasTypes[1])
		{
			start_hp = 10;
			hp_dices.setEdge(10);
			simple_weapon_training = true;
			warior_weapon_training = true;
			if (name == "Ranger")
			{
				medium_armour_training = true;
				light_armour_training = true;
				
			}

			else
			{
				heavy_armour_training = true;
				medium_armour_training = true;
				light_armour_training = true;
			}
				
		}

		else if (class_type == ClasTypes[2])
		{
			start_hp = 8;
			hp_dices.setEdge(8);
			simple_weapon_training = true;
			if (name!="Monk")
				light_armour_training = true;
			if (name == "Cleric")
				medium_armour_training = true;

		}

		else if (class_type == ClasTypes[3])
		{
			start_hp = 6;
			hp_dices.setEdge(6);
		}

	}



};


